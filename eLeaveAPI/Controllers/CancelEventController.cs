﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eLeaveAPI.Helper;
using eLeaveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eLeaveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CancelEventController : ControllerBase
    {
        readonly DBContext _context;
        public CancelEventController(DBContext context)
        {
            _context = context;
        }


        // POST: api/CancelEvent
        [HttpPost]
        public IActionResult Post(int eventId)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var result = _context.Events.FirstOrDefault(o => o.Id == eventId && o.statusId == (int)EnumHelper.Status.Approve);

                    result.Id = eventId;
                    result.statusId = (int)EnumHelper.Status.Cancel;
                    result.Updated_at = DateTime.Now;
                    _context.Events.Update(result);

                    var usage = _context.Usages.FirstOrDefault(o => o.userId == result.userId);

                    usage.userId = result.userId;

                    if (result.Topic == (int)EnumHelper.Refer.Hiliday)
                    {
                        usage.Holiday = (usage.Holiday + result.Amount);
                    }
                    else if (result.Topic == (int)EnumHelper.Refer.Personal)
                    {
                        usage.Personal = (usage.Personal + result.Amount);
                    }
                    else if (result.Topic == (int)EnumHelper.Refer.Sick)
                    {
                        usage.Sick = (usage.Sick + result.Amount);
                    }
                    usage.Updated_at = DateTime.Now;
                    _context.Usages.Update(usage);
                    _context.SaveChanges();
                    transaction.Commit();
                    return Ok();
                }
                catch
                {
                    transaction.Rollback();
                    return BadRequest();
                }
            }

        }

    }
}
