﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eLeaveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eLeaveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalandByUserController : ControllerBase
    {
        readonly DBContext _context;
        public BalandByUserController(DBContext context)
        {
            _context = context;
        }


        // POST: api/BalandByUser
        [HttpPost]
        public IActionResult Post(setBaland? sb)
        {
            var result = _context.Usages.FirstOrDefault(o => o.userId == sb.userId);

            result.userId = sb.userId;
            result.Sick = sb.Sick;
            result.Personal = sb.Personal;
            result.Holiday = sb.Holiday;
            result.Updated_at = DateTime.Now;
            _context.Usages.Update(result);
            _context.SaveChanges();
            return Ok();
        }
    }
}
