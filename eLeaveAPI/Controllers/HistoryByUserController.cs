﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eLeaveAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace eLeaveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryByUserController : ControllerBase
    {
        readonly DBContext _context;
        public HistoryByUserController(DBContext context)
        {
            _context = context;
        }
        // GET: api/HistoryByUser
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/HistoryByUser/5
        [HttpGet("{userId}", Name = "GetHistoryByUser")]
        public IEnumerable<vw_event> GetHistoryByUser(int userId)
        {
            return _context.vw_events.Where(o => o.userId == userId).ToList();
        }
    }
}
